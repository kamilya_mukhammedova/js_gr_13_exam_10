import { Injectable } from '@angular/core';
import { map, Subject } from 'rxjs';
import { Post, PostData } from '../models/post.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Comment, CommentData } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  postsChange = new Subject<Post[]>();
  commentsChange = new Subject<Comment[]>();
  private postsArray: Post[] = [];
  private commentsArray: Comment[] = [];

  constructor(private http: HttpClient) { }

  getPosts() {
    this.http.get<Post[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(newsData => {
          return new Post(
            newsData.id,
            newsData.title,
            newsData.content,
            newsData.image,
            newsData.date,
          );
        });
      }))
      .subscribe({
        next: news => {
          this.postsArray = news;
          this.postsChange.next(this.postsArray.slice());
        }
      });
  }

  addNewPost(postData: PostData) {
   const formData = new FormData();
    Object.keys(postData).forEach(key => {
      if(postData[key] !== null) {
        formData.append(key, postData[key]);
      }
    });
    return this.http.post(environment.apiUrl + '/news', formData);
  }

  addNewComment(commentData: CommentData) {
    const formData = new FormData();
    Object.keys(commentData).forEach(key => {
      if(commentData[key] !== null) {
        formData.append(key, commentData[key]);
      }
    });
    return this.http.post(environment.apiUrl + '/comments', formData);
  }

  getPostById(id: number) {
    return this.http.get<Post | null>(environment.apiUrl + `/news/${id}`).pipe(
      map(result => {
        if(!result) {
          return null;
        }
        return new Post(
          result.id,
          result.title,
          result.content,
          result.image,
          result.date,
        );
      }));
  }

  getComments(news_id: number) {
    return this.http.get<Comment[]>(environment.apiUrl + `/comments?news_id=${news_id}`).pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(result => {
          return new Comment(
            result.id,
            result.news_id,
            result.author,
            result.content,
          );
        });
      }))
      .subscribe({
        next: comments => {
          this.commentsArray = comments;
          this.commentsChange.next(this.commentsArray.slice());
        }
      });
  }

  removeComment(commentId: number) {
    return this.http.delete(environment.apiUrl + `/comments/${commentId}`);
  }

  removePost(postId: number) {
    return this.http.delete(environment.apiUrl + `/news/${postId}`);
  }
}
