import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Post } from '../models/post.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit, OnDestroy {
  posts!: Post[];
  postsChangeSubscription!: Subscription;

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    this.postsChangeSubscription = this.postsService.postsChange.subscribe((posts: Post[]) => {
      this.posts = posts.reverse();
    });
    this.postsService.getPosts();
  }

  onRemovingPost(id: number) {
    this.postsService.removePost(id).subscribe(() => {
      this.postsService.getPosts();
    });
  }

  ngOnDestroy(): void {
    this.postsChangeSubscription.unsubscribe();
  }
}
