import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Post } from '../models/post.model';
import { Comment } from '../models/comment.model';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.sass']
})
export class PostDetailsComponent implements OnInit , OnDestroy{
  post!: Post | null;
  comments!: Comment[];
  commentsChangeSubscription!: Subscription;
  postId = 0;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.commentsChangeSubscription = this.postsService.commentsChange.subscribe((comments: Comment[]) => {
      this.comments = comments;
    });
    this.route.params.subscribe((params:Params) => {
      this.postId = parseInt(params['id']);
      this.postsService.getPostById(this.postId).subscribe(post => {
        this.post = post;
      });
      this.postsService.getComments(this.postId);
    });
  }

  onRemoveComment(id: number) {
    this.postsService.removeComment(id).subscribe(() => {
      this.postsService.getComments(this.postId);
    });
  }

  ngOnDestroy(): void {
    this.commentsChangeSubscription.unsubscribe();
  }
}
