import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostsService } from '../services/posts.service';
import { PostData } from '../models/post.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.sass']
})
export class AddPostComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(
    private postsService: PostsService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
   const postData: PostData = this.form.value;
   this.postsService.addNewPost(postData).subscribe(() => {
     void this.router.navigate(['/']);
   });
  }
}
