import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostsService } from '../services/posts.service';
import { CommentData } from '../models/comment.model';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.sass']
})
export class AddCommentComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  postId = 0;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = parseInt(params['id']);
    });
  }

  onSubmit() {
    const commentData: CommentData = this.form.value;
    commentData['news_id'] = this.postId;
    console.log(commentData)
    this.postsService.addNewComment(commentData).subscribe(() => {
      this.postsService.getComments(this.postId);
    });
  }
}
