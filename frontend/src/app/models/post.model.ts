export class Post {
  constructor(
    public id: number,
    public title: string,
    public content: string,
    public image: string,
    public date: string,
  ) {}
}

export interface PostData {
  [key: string]: any;
  title: string;
  content: string;
  image: File | null;
}
