export class Comment {
  constructor(
    public id: number,
    public news_id: number,
    public author: string,
    public content: string,
  ) {}
}

export interface CommentData {
  [key: string]: any;
  author: string;
  content: string;
}
