const express = require('express');
const db = require('../mySqlDb');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    let query = `SELECT * FROM comments WHERE news_id = ${req.query.news_id}`;
    let [comments] = await db.getConnection().execute(query);
    return res.send(comments);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.content) {
      return res.status(400).send({message: 'Content of comment is required'});
    }
      const newComment = {
        author: req.body.author ? req.body.author : 'Anonymous',
        content: req.body.content,
        news_id: req.body.news_id,
      };
      let query = 'INSERT INTO comments (news_id, author, content) VALUES (?,?,?)';
      const [results] = await db.getConnection().execute(query, [
        newComment.author,
        newComment.content,
        newComment.news_id,
      ]);
      const id = results.insertId;
      return res.send({message: 'Created new comment', id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.getConnection().execute('DELETE FROM comments WHERE id = ?', [req.params.id]);
    return res.send({message: `Comment with id ${req.params.id} has been deleted`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;