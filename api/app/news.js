const express = require('express');
const db = require('../mySqlDb');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT * FROM news';
    let [news] = await db.getConnection().execute(query);
    for(let i = 0; i < news.length; i++) {
      delete news[i].content;
    }
    return res.send(news);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const [news] = await db.getConnection().execute('SELECT * FROM news WHERE id = ?', [req.params.id]);
    const oneNews = news[0];
    if (!oneNews) {
      return res.status(404).send({message: 'News is not found'});
    }
    return res.send(oneNews);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if(!req.body.title || !req.body.content) {
      return res.status(400).send({message: 'Title of news and content are required'});
    }
    const breakingNews = {
      title: req.body.title,
      content: req.body.content,
      image: null,
      date: new Date().toISOString(),
    };
    if(req.file) {
      breakingNews.image = req.file.filename;
    }

    let query = 'INSERT INTO news (title, content, image, date) VALUES (?,?,?,?)';
    const [results] = await db.getConnection().execute(query, [
      breakingNews.title,
      breakingNews.content,
      breakingNews.image,
      breakingNews.date,
    ]);
    const id = results.insertId;
    return res.send({message: 'Created breaking news', id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.getConnection().execute('DELETE FROM news WHERE id = ?', [req.params.id]);
    return res.send({message: `News with id ${req.params.id} has been deleted`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;